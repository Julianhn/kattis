import sys
import math

for line in sys.stdin:
    l = line.split()

    a = int(l[0])
    b = int(l[1])
    s = int(l[2])
    m = int(l[3])
    n = int(l[4])

    if (a|b|s|m|n == 0):
        sys.exit(0)

    side_o = b*n
    side_a = a*m
    side_h = math.sqrt((side_o**2) + (side_a**2))

    speed = side_h / s
    
    angle = math.degrees(math.atan2(side_o, side_a))
        
    print('{0:.2f}'.format(angle),'{0:.2f}'.format(speed))

    
